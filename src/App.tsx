import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';

import Home from './pages/home/Home';

import {TodoContext, initialTodos, saveTodos} from './AppContext';

function App() {
    const [todos, setTodos] = React.useState(initialTodos);

    const toggleTodo: ToggleTodo = (selectedTodo: Todo) => {
        const newTodos = todos.map(todo => {
            if (todo === selectedTodo) {
                return {
                    ...todo,
                    complete: !todo.complete,
                };
            }
            return todo;
        });
        saveTodos(newTodos);
        setTodos(newTodos);
    };

    const deleteTodo: DeleteTodo = (id: string) => {
        const filteredTodo = todos.filter(todo => todo.id !== id);
        saveTodos(filteredTodo);
        setTodos(filteredTodo);
    };

    const addTodo: AddTodo = (text: string) => {
        const newTodo = { id: uuidv4(), text, complete: false, };
        const newTodos = [...todos, newTodo];
        saveTodos(newTodos);
        setTodos(newTodos);
    };

    return (
        <TodoContext.Provider value={{todos, setTodos, toggleTodo, deleteTodo, addTodo}}>
            <div className="App">
                <Home/>
            </div>
        </TodoContext.Provider>
    );
}

export default App;
