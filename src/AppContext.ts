import { createContext, useContext } from 'react';

const tryFetchTodos  = () : Todo[] => {
    let todos = [] as Todo[];

    const localStorageString = localStorage.getItem("todos");
    if (localStorageString != null) {
        todos = JSON.parse(localStorageString);
    }

    return todos;
};

export const saveTodos = (todos : Todo[]) => {
    const json = JSON.stringify(todos);
    localStorage.setItem("todos", json);
};

export const initialTodos: Todo[] = tryFetchTodos();

export type TodoContextType = {
    todos: Todo[];
    toggleTodo: ToggleTodo;
    deleteTodo: DeleteTodo;
    addTodo: AddTodo;
    setTodos: SetTodos;
}

export const TodoContext = createContext<TodoContextType>({ todos: initialTodos, toggleTodo: () => {}, addTodo: () => {}, deleteTodo: () => {}, setTodos: () => {}});

export const useTodos = () => useContext(TodoContext);
