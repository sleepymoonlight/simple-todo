import React from 'react';

import TodoContainer from '../../containers/todo/TodoContainer';
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';


const Home = () => {
    return (
        <>
            <Header title="Simple Todo App" />
            <TodoContainer />
            <Footer />
        </>
    )
};

export default Home;
