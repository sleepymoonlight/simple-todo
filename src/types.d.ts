interface Todo {
    id: string;
    text: string;
    complete: boolean;
}

type ToggleTodo = (selectedTodo: Todo) => void;

type DeleteTodo = (id: string) => void;

type AddTodo = (text: string) => void;

type SetTodos = (todos: Todo[]) => void;
