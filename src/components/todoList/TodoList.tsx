import React from 'react';
import List from '@material-ui/core/List';
import {makeStyles} from '@material-ui/core/styles';

import {TodoListItem} from '../todoListItem/TodoListItem';

import {useTodos} from '../../AppContext';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));

export const TodoList = () => {
    const { todos } = useTodos();
    const classes = useStyles();

    return (
        <List dense className={classes.root}>
            {
                todos.map(todo => (
                    <TodoListItem
                        key={todo.text}
                        todo={todo}
                    />
                ))
            }
        </List>
    );
};