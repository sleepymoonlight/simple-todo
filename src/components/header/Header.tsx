import React from 'react';

import './styles.scss';

interface Props {
    title: string;
}

const Header: React.FC<Props> = ({title}) => {
    return (
        <div className="app-header">
            <div className="header-container">{title}</div>
        </div>
    )
};

export default Header;
