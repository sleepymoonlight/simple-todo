import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import {makeStyles} from '@material-ui/core/styles';

import {useTodos} from '../../AppContext';

import './styles.scss';

interface Props {
    todo: Todo;
}

const useStyles = makeStyles({
    root: {
        color: '#333',
        fontFamily: 'Arial',
        height: 50,
        padding: '0 15px 0 30px',
        margin: '10px 0',
        boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
    },
});

export const TodoListItem: React.FC<Props> = ({todo}) => {
    const { toggleTodo, deleteTodo } = useTodos();
    const classes = useStyles();

    return (
        <ListItem
            key={todo.text}
            button
            classes={{
                root: classes.root,
            }}
            onClick={() => toggleTodo(todo)}
        >
            <ListItemText id={todo.text} primary={todo.text}/>
            <ListItemSecondaryAction>
                <Checkbox
                    edge="end"
                    onChange={() => toggleTodo(todo)}
                    checked={todo.complete}
                    inputProps={{'aria-labelledby': todo.text}}
                />
                <IconButton aria-label="delete" onClick={() => deleteTodo(todo.id)}>
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    );
};
