import React, {useState} from 'react';
import {Button, TextField} from '@material-ui/core';
import {Save} from '@material-ui/icons';

import {useTodos} from '../../AppContext';

import './styles.scss';

export const AddTodoForm = () => {
    const {addTodo} = useTodos();
    const [text, setText] = useState('');

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        addTodo(text);
        setText('')
    };

    return (
        <div className="add-todo-form-container">
            <form className="add-todo-form" noValidate autoComplete="off" onSubmit={handleSubmit}>
                <TextField
                    id="outlined-basic"
                    label="TodoText"
                    variant="outlined"
                    value={text}
                    onChange={e => setText(e.target.value)}
                />
                <Button
                    variant="contained"
                    size="large"
                    startIcon={<Save/>}
                    type="submit"
                >
                    Add Todo
                </Button>
            </form>
        </div>
    );
};

