import React from 'react';

import './styles.scss';

const Footer = () => {
    return (
        <div className="app-footer">
            <div className="footer-container">Copyright © 2021 All Rights Reserved.</div>
        </div>
    )
};

export default Footer;
