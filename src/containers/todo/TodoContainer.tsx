import React  from 'react';

import { TodoList } from '../../components/todoList/TodoList';
import { AddTodoForm } from '../../components/addTodoForm/AddTodoForm';

import { useTodos } from '../../AppContext';

import './styles.scss';

const TodoContainer = () => {
    const { todos, setTodos } = useTodos();

    const addTodo: AddTodo = (text: string) => {
        const newTodo = { text, complete: false, id: text };
        setTodos([...todos, newTodo]);
    };

    return (
        <div className="todos-container">
            <div className="todos-box">
                <AddTodoForm addTodo={addTodo} />
                <TodoList />
            </div>
        </div>
    );
};

export default TodoContainer;
